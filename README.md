# OdinEditorMenu

Example to check the union of OdinEditor and OdinMenuEditor in a window.

# Requirements

- [Unity][1]
- [Odin][2]

# Prev

![01](https://gitlab.com/MoonAntonio/odineditormenu/raw/master/res/prev.png)

[1]: https://unity3d.com/es
[2]: https://sirenix.net/