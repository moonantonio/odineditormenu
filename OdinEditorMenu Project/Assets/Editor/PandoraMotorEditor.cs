﻿//                                  ┌∩┐(◣_◢)┌∩┐
//																				\\
// PandoraMotorEditor.cs (30/09/2016)											\\
// Autor: Antonio Mateo (.\Moon Antonio) 	antoniomt.moon@gmail.com			\\
// Descripcion:		Motor para gestionar todo el contenido.						\\
// Fecha Mod:		30/09/2016													\\
// Ultima Mod:		GUI principal.												\\
//******************************************************************************\\

#region Librerias
using Sirenix.OdinInspector.Editor;
using UnityEditor;
using Sirenix.OdinInspector;
using UnityEngine;
using Sirenix.Utilities.Editor;
using Sirenix.OdinInspector.Demos.RPGEditor;
using System.Collections.Generic;
using System.Linq;
using Sirenix.Utilities;
using Sirenix.Serialization;
using Sirenix.OdinInspector.Demos;
#endregion

namespace Pandora
{
	/// <summary>
	/// <para>Motor para gestionar todo el contenido.</para>
	/// </summary>
	public class PandoraMotorEditor : OdinEditorWindow
	{
		#region Menu
		[MenuItem("Pandora/Pandora Editor")]
		private static void OpenWindow()
		{
			Texture2D t = EditorGUIUtility.Load("Pandora/panIcon.png") as Texture2D;

			if (t == null) Debug.LogWarning("PandoraMotorEditor :: OpenWindows >> No se encuentra el icono.");

			var win = GetWindow<PandoraMotorEditor>();
			win.position = GUIHelper.GetEditorWindowRect().AlignCenter(270, 200);
			win.titleContent = new GUIContent("Pandora", t);
			win.Show();
		}
		#endregion

		#region Inicializadores
		protected override void OnEnable()
		{

		}

		public string Combined { get { return "v.1.0 - " + Application.productName + " - " + Application.companyName; } }
		#endregion

		#region Enums
		public enum CategoriasGenerales
		{
			Main, Game, Stats, Jugador, Unidades, Items, Combate, QuestLine
		}
		#endregion

		#region Header
		[HideLabel, MinMaxSlider(-100, 100), TitleGroup("Pandora Crystal", "$Combined", alignment: TitleAlignments.Centered, horizontalLine: true, boldTitle: true, indent: false)]
		public Vector2 Zero;

		[TabGroup("Main"), HideLabel]
		public MainEditor Main = new MainEditor();

		[TabGroup("Game")]
		public GameEditor Game;

		[TabGroup("Stats")]
		public StatsEditor Stats;

		[TabGroup("Jugador")]
		public JugadorEditor Jugador;

		[TabGroup("Unidades")]
		public UnidadesEditor Unidades;

		[TabGroup("Items")]
		public ItemsEditor Items;

		[TabGroup("Combate")]
		public CombateEditor Combate;

		[TabGroup("QuestLine")]
		public QuestLineEditor QuestLine;
		#endregion
	}

	[System.Serializable]
	public class MainEditor
	{
		[HorizontalGroup("Ajustes de Pandora", LabelWidth = 80), DisplayAsString, InfoBox("Esta seccion se encarga del setup general del motor Pandora, entre otras cosas, sus datos."), HideLabel]
		public string Descripcion = "";

		[OnInspectorGUI]
		public RPGEditorWindow windRPG;
		
	}

	[System.Serializable]
	public class GameEditor
	{

	}

	[System.Serializable]
	public class StatsEditor
	{
	}

	[System.Serializable]
	public class JugadorEditor
	{
	}

	[System.Serializable]
	public class UnidadesEditor
	{
	}

	[System.Serializable]
	public class ItemsEditor
	{
	}

	[System.Serializable]
	public class CombateEditor
	{
	}

	[System.Serializable]
	public class QuestLineEditor
	{
	}
}